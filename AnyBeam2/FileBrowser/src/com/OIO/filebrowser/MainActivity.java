package com.OIO.filebrowser;

import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;



import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MainActivity extends Activity {
	
	
	
	String path;
	String fileName;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		
			path=Environment.getExternalStorageDirectory()+File.separator;
		
		
		
		
		
		 
	   
		
	makeDirectoryList(path);
	
		

	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		
		return true;
	}
	
	public void makeDirectoryList(String directory){
		final ListView myList=(ListView)findViewById(R.id.dirArea);
		File dirManager = new File(directory);
		ArrayList<String> numberOfDirs=new ArrayList<String>(Arrays.asList(dirManager.list()));
		ArrayList<String> listDirectories=new ArrayList<String>();
		ArrayAdapter<String> adaptDirs=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listDirectories);
		myList.setAdapter(adaptDirs);
		String dirName;
		
		File [] toAdd=dirManager.listFiles();
		
		dirName="/";
		listDirectories.add(dirName);
		dirName="../";
		listDirectories.add(dirName);
		
		
		int i=0;
		while(i<(numberOfDirs.size())){
			boolean isDir;
			
			isDir=toAdd[i].isDirectory();
			if(isDir==true){
				dirName=toAdd[i].getName()+ "/";
				listDirectories.add(dirName);
			}
			adaptDirs.notifyDataSetChanged();
			
			i++;
		}
		makeListofFiles(path);
		myList.setClickable(true);
		myList.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
		 

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				File thisPath=new File(path);
				arg0=myList;
				
		       String get=(String) arg0.getItemAtPosition(arg2);
		       if(get=="../"){
		    	   path=thisPath.getParent()+File.separator;
		       }
		       
		       else if(get=="/"){
		    	   path=Environment.getExternalStorageDirectory()+File.separator;
		       }
		       else{
		    	   path=path+get;
		       }   
		       Context something=MainActivity.this;
		       CharSequence toastWords=path;
		       Toast thisDirectory=Toast.makeText(something,toastWords,Toast.LENGTH_SHORT);
		       thisDirectory.show();
		       makeDirectoryList(path);
		        }
	    });
	}
		
	
	public void makeListofFiles(String directory){
		final RadioGroup myRadioGroup=(RadioGroup) findViewById(R.id.fileArea);
		File manager =new File(directory);
		ArrayList<String> listFiles=new ArrayList<String>(Arrays.asList(manager.list()));
		File [] setofFiles=manager.listFiles();
		myRadioGroup.removeAllViews();
		
		
		int n=0;
		int o=listFiles.size();
		int p=0;
		
		while(n<o){
			
			RadioButton myRadioButton=new RadioButton(this);
			myRadioButton.setTextSize(20);
			myRadioButton.setPadding(10,30,10,30);
			if (!setofFiles[n].isDirectory()){
				myRadioGroup.addView(myRadioButton, p);
				myRadioButton.setText(listFiles.get(n).toString());
				p++;
			}
			n++;
		}
		myRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

		    @Override
		    public void onCheckedChanged(RadioGroup rg, int checkedId) 
		    {
		        RadioButton aButton=(RadioButton)findViewById(checkedId);
		    	String txt = path+aButton.getText().toString();
		    	
		    	loadCargo(txt);
		    }
		});
	
		}
	
		public void loadCargo(String filePath){
			NfcAdapter myNfcAdapter = NfcAdapter.getDefaultAdapter(this);
			File cargo=new File(filePath);
			
			Uri toPush=Uri.fromFile(cargo);
			Toast.makeText(getBaseContext(),(String)toPush.toString(), 
	                Toast.LENGTH_SHORT).show();
			myNfcAdapter.setBeamPushUris(new Uri [] {toPush},this);
		}
	}

